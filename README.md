# Practical exercise

## Dataset

The dataset chosen for this course is the Real Estate prices dataset, which can be downloaded from [here](https://www.kaggle.com/quantbruce/real-estate-price-prediction). This is correct now.

The dataset independent variables are:

- `X1_transaction_date`: the transaction date (for example, 2013.250=2013 March, 2013.500=2013 June, etc.).
- `X2_house_age`: the house age (unit: year).
- `X3_distance_to_the_nearest_MRT_station`: the distance to the nearest MRT station (unit: meter).
- `X4_number_of_convenience_stores`: the number of convenience stores in the living circle on foot (integer).
- `X5_latitude`: the geographic coordinate, latitude. (unit: degree).
- `X6_longitude`: the geographic coordinate, longitude. (unit: degree)

And the target variable is:

- `Y_house_price_of_unit_area`: house price of unit area (10000 New Taiwan Dollar/Ping, where Ping is a local unit, 1 Ping = 3.3 meter squared)

## Linear Regression

### **Training**

We can train a linear regression model in BigQuery ML with the following SQL query:

```sql
CREATE OR REPLACE MODEL `ms-gauss-pixel.course_exercise_test.real_estate_lreg`
OPTIONS(
    MODEL_TYPE='LINEAR_REG',
    MAX_ITERATIONS=5,
    INPUT_LABEL_COLS=['Y_house_price_of_unit_area'],
    DATA_SPLIT_METHOD='RANDOM',
    DATA_SPLIT_EVAL_FRACTION=0.2
) AS
SELECT
* EXCEPT(`No`)
FROM `ms-gauss-pixel.course_exercise_test.real_estate`
```

This query trains a linear regression model named `real_estate_lreg` using the data in the table addressed in the query statement. In this case, it would be the table `real_estate` in the dataset `course_exercise_test` in project `ms-gauss-pixel`.

### **Evaluation**

Once the model has been trained we can gather evaluation metrics from the

```sql
SELECT
*
FROM ML.EVALUATE(MODEL `ms-gauss-pixel.course_exercise_test.real_estate_lreg`)
```

```json
{
  "mean_absolute_error": "6.1307499389903075",
  "mean_squared_error": "77.13171720187219",
  "mean_squared_log_error": "0.06403312071347379",
  "median_absolute_error": "4.970234461141157",
  "r2_score": "0.582370447272307",
  "explained_variance": "0.5823704472723077"
}
```

- `mean_absolute_error`: The average distance from the predicted value to the actual value. Lower is better.
- `mean_squared_error`: Used for evaluating statistical significance. Lower is better.
- `mean_squared_log_error`: Used as a numerically stable cost function by Gradient Descent for training the model. Lower is better.
- `median_absolute_error`: A measure more robust to outliers. Lower is better.
- `r2_score`: Coefficient of determination. Higher is better.
- `explained_variance`: The fraction of variance explained. Higher is better.

### **Model Explanation**

By running the following query we can examine the impact of the house's age variable in the final result:

```sql
SELECT
fi.input,fi.mean,fi.stddev,w.weight
FROM

# Info for destandardizing
ML.FEATURE_INFO(MODEL `ms-gauss-pixel.course_exercise_test.real_estate_lreg`) fi
INNER JOIN

# Model coefficients
ML.WEIGHTS(MODEL `ms-gauss-pixel.course_exercise_test.real_estate_lreg`) w
ON
fi.input = w.processed_input
WHERE
fi.input = 'X2_house_age'
```

The functions used in this query are:

- `ML.FEATURE_INFO`: allows you to see information about the input features used to train a model.
- `ML.WEIGHTS`: allows you to see the underlying weights used by a model during prediction.

Query result:

```json
{
  "input": "X2_house_age",
  "mean": "17.712560386473406",
  "stddev": "11.392484533242529",
  "weight": "-0.2696954475804437"
}
```

We can explain this as follows:

All else being equal, for every 11.39 (stddev) years the house age is above 17.71 (mean) years old, we would expect to see the price per unit area of the house decrease by 0.269 (weight) dollars.

## Clustering

### **Create Clustering Model**

Use this query to create a clustering model using K-means:

```sql
CREATE OR REPLACE MODEL
  `ms-gauss-pixel.course_exercise_test.real_estate_kmeans`
OPTIONS
  ( MODEL_TYPE='KMEANS')
AS
SELECT
  * EXCEPT(`No`)
FROM `ms-gauss-pixel.course_exercise_test.real_estate`
```

Optionally, we could add a model option `NUM_CLUSTERS` to indicate the number of clusters we want in k-means.

### **Evaluate**

Use this query to get evaluation metrics from the model:

```sql
SELECT
*
FROM
ML.EVALUATE(MODEL `ms-gauss-pixel.course_exercise_test.real_estate_kmeans`)
```

These are the model's metrics:

```json
{
  "davies_bouldin_index": "0.8928623864852152",
  "mean_squared_distance": "5.065937784241262"
}
```
